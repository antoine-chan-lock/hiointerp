# HiOInterp



## Data generation

This repository contains the code for the data generation of the paper "[High Order Elasticity Interpolants for Microstructure Simulation](http://mslab.es/projects/HiOInterp/)" by Antoine Chan-Lock, Jesus Perez and Miguel Otaduy.

It contains the following files:
- `genHexMesh.m`: Generates the 2D mesh for the microstructure 10
- `genPBClist2D.m`: Generates the list of periodic boundary conditions for the microstructure 10
- `dscadve.m`: Generates derivatives code in C++
- `visu.m`: Shows the generated data, and the final data distubution similar to M10 in Table 6 of the paper

The data generation is written in C++ using Eigen in the folder DataGeneration. To compile and execute:
```
cd ./DataGeneration
mkdir build
cd ./build
cmake ..
make
./hiointerp
```
The data is already generated and stored in the folder DataGenetation/data/10. You can compile and run the code with no additional dependencies.