//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

#pragma once
#include "CommonIncludes.h"
#include "Material.h"

// Element geometry computation. We use here 2D hexahedrons
class Element
{
public:
    Element();
    Element(int id, const DenseMatrixXi &connectivity, const DenseMatrixX &Xrest);
    virtual ~Element();
    virtual Vec8iT getIndexDoFs() const; // Index of the dofs in the global vector
    virtual double getEnergy(const DenseMatrixX &Xcurrent) const;
    virtual Vec8T getGradient(const DenseMatrixX &Xcurrent) const;
    virtual Matrix8 getHessian(const DenseMatrixX &Xcurrent) const;
    virtual int checkInversion(const DenseMatrixX &Xcurrent) const;

protected:
    int m_id, m_nbNodes, m_nbDofs;
    double m_detJac[4];
    Eigen::Matrix<double, 4, 2> m_dN[4];     // Shape functions derivatives
    Eigen::Matrix<double, 4, 2> m_dFdx[4];   // Deformation gradient derivatives
    Eigen::Matrix<double, 4, 8> m_dvFdvx[4]; // Deformation gradient derivatives vectorized
    Eigen::Matrix<int, 1, 4> m_nodes;        // Connectivity
    Eigen::Matrix<double, 2, 4> m_Xrest;     // Rest configuration
    Material m_material;
};
