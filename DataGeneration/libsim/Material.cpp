//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

#include "Material.h"

Material::Material()
{
}

void Material::init(double young, double nu)
{
    // Material parameters
    m_young = young;
    m_nu = nu;
    m_G = 0.5 * m_young / (1.0 + m_nu);
    m_lambda = (m_young * m_nu) / ((1.0 + m_nu) * (1.0 - 1.0 * m_nu));
    // Reparameterization in 2D
    m_lambdaNH = m_lambda + (2.0 / 3.0) * m_G;
    m_GNH = (3.0 / 2.0) * m_G;
    m_alpha = 1.0 + (2.0 / 3.0) * (m_GNH / m_lambdaNH);
}

Material::~Material()
{
}

double Material::psi(const Matrix2 &F) const
{
    // Input: Deformation gradient F
    // Output: Energy density
    Matrix2 C = F.transpose() * F;
    double IC = C.trace();
    double J = F.determinant();
    return (m_GNH / 2) * (IC - 3) + (m_lambdaNH / 2) * pow(J - m_alpha, 2) - (m_GNH / 2) * log(IC + 1);
}

Vec4T Material::dpsidF(const Matrix2 &F) const
{
    // Input: Deformation gradient F
    // Output: Gradient of energy density w.r.t. F
    Matrix2 C = F.transpose() * F;
    double IC = C.trace();
    Vec4T vF, dJdF, dICdF;
    double J = F.determinant();
    vF << F(0, 0), F(1, 0), F(0, 1), F(1, 1);
    dICdF = 2 * vF;
    dJdF << F(1, 1), -F(0, 1), -F(1, 0), F(0, 0);
    return (m_GNH / 2) * dICdF + (m_lambdaNH / 2) * 2 * (J - m_alpha) * dJdF - (m_GNH / 2) / (IC + 1) * dICdF;
}

Matrix4 Material::dpsidFF(const Matrix2 &F) const
{
    // Input: Deformation gradient F
    // Output: Hessian of energy density w.r.t. F
    Matrix4 dICdFF, dJdFF;
    Vec4T vF, dICdF, dJdF;
    Matrix2 C = F.transpose() * F;
    vF << F(0, 0), F(1, 0), F(0, 1), F(1, 1);
    double J = F.determinant();
    dICdF = 2 * vF;
    double IC = C.trace();
    IC = C.trace();
    dJdFF << 0, 0, 0, 1,
        0, 0, -1, 0,
        0, -1, 0, 0,
        1, 0, 0, 0;
    dJdF << F(1, 1), -F(0, 1), -F(1, 0), F(0, 0);
    dICdFF.setIdentity();
    dICdFF = dICdFF * 2;
    Matrix4 H;
    H = (m_GNH / 2) * dICdFF + (m_lambdaNH / 2) * (2 * (J - m_alpha) * dJdFF + dJdF.transpose() * dJdF * 2) - (m_GNH / 2) * (1 / (IC + 1) * dICdFF - dICdF.transpose() * dICdF / (pow(IC + 1, 2)));
    return clamp(H);
}

Matrix4 Material::clamp(const Matrix4 &H) const
{
    // Input: Matrix H
    // Output: Matrix H with negative eigenvalues set to zero (Convex Energy Projection)
    Eigen::EigenSolver<Matrix4> es((H + H.transpose()) / 2);

    Matrix4 D = es.eigenvalues().real().asDiagonal();
    Matrix4 V = es.eigenvectors().real();
    for (int i = 0; i < 4; i++)
        if (D(i, i) < 0)
            D(i, i) = 0;
    Matrix4 res = V * D * V.transpose();
    if (std::isnan(res.norm()))
    {
        std::cout << H << std::endl;
        std::cout << "--" << std::endl;
        std::cout << D << std::endl;
        std::cout << "--" << std::endl;
        std::cout << V << std::endl;
        std::cout << "--" << std::endl;
        std::cout << res << std::endl;
        std::cout << "nan alert" << std::endl;
        exit(0);
    }
    return res;
}
