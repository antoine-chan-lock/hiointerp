//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

// #include "Tools.h"
// #include "Material.h"
// #include "Element.h"
// #include "Object.h"
#include "DataGen.h"

int main(int argc, char *argv[])
{
    DataGen datagen("./../data/10");
    datagen.genBenStretchData();
    return 0;
}
