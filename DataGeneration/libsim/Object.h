//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

#pragma once
#include "CommonIncludes.h"
#include "Tools.h"
#include "Element.h"

// We simulate microstructures with 2D hexahedral meshes.
// Here we compute its material response, set boundary conditions, and solve for the deformation.
// Periodic boundary conditions simulations are realized as described in Homogenized Yarn-Level Cloth, G. Sperl.
// The current confuguration X is defined as X = Xfine + Xcoarse.
// We apply a linear transformation on Xcoarse
// We solve Xfine s.t. it satisfies the boundary conditions and minimizes the energy.
class Object
{
public:
    Object();
    Object(std::string matdir);
    ~Object();

    // Material + BC metrics
    virtual double getEnergy(const DenseMatrixX &Xfine) const;
    double getEnergy(const VectorX &vXfine) const;
    virtual VectorX getGradient(const DenseMatrixX &Xfine) const;
    SparseMatrixX getHessian(const DenseMatrixX &Xfine) const;

    // Material metrics
    double getEnergyMembrane(const DenseMatrixX &X) const;
    VectorX getGradientMembrane(const DenseMatrixX &X) const;
    SparseMatrixX getHessianMembrane(const DenseMatrixX &X) const;

    // Boundary conditions metrics
    double penalty(const VectorX &vXfine) const;
    VectorX dpenalty(const VectorX &vXfine) const;
    SparseMatrixX ddpenalty(const VectorX &vXfine) const;

    // Misc
    DenseMatrixX devectorize(const VectorX &vX) const;
    VectorX vectorize(const DenseMatrixX &X) const;
    bool checkInversion(const DenseMatrixX &Xfine) const;

    // Getters
    DenseMatrixXi getConnetivity() const;

    // Solver
    virtual VectorX NewtonRaphson(const VectorX &vXfine) const;

    void updateXcoarseF(const Matrix2 &F);
    void updateXcoarseSCA(const Vec3 &sca);
    DenseMatrixX getXcoarse() const;

protected:
    DenseMatrixXi m_connectivity;    // Connectivity matrix
    int m_nbNodes;                   // Number of nodes
    int m_nbElements;                // Number of elements
    int m_nbDoFs;                    // Number of degrees of freedom
    DenseMatrixX m_restX, m_coarseX; // Rest and coarse configurations
    Vec3 m_sca;                      // Scaling factors for the coarse linear transformation
    std::vector<Element> m_velement; // Vector of elements
    DenseMatrixXi m_PBCDoFs;         // Periodic boundary conditions
};
