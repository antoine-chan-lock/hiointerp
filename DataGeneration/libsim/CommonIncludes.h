//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <vector>
#include <numeric>   // std::iota
#include <algorithm> // std::sort, std::stable_sort
#include <omp.h>

using Vec3 = Eigen::Matrix<double, 3, 1>;
using Vec4T = Eigen::Matrix<double, 1, 4>;
using Vec8T = Eigen::Matrix<double, 1, 8>;
using VectorX = Eigen::Matrix<double, Eigen::Dynamic, 1>;
using Vec8iT = Eigen::Matrix<int, 1, 8>;
using Matrix2 = Eigen::Matrix<double, 2, 2>;
using Matrix3 = Eigen::Matrix<double, 3, 3>;
using Matrix4 = Eigen::Matrix<double, 4, 4>;
using Matrix8 = Eigen::Matrix<double, 8, 8>;
using Matrix2_4 = Eigen::Matrix<double, 2, 4>;
using DenseMatrixX = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>;
using DenseMatrixXi = Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic>;
using SparseMatrixX = Eigen::SparseMatrix<double>;
