//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

#pragma once
#include "Object.h"

class DataGen
{
public:
    DataGen(const std::string &matdir);
    ~DataGen();

    double energy(const Vec3 &sca) const;
    Vec3 dpsidsca(const Vec3 &sca) const;
    Vec3 SCA2vE(const Vec3 &sca) const;
    Matrix3 dscadvE(const Vec3 &sca) const;

    double gradDesc(const Vec3 &sca_in) const;
    void genBenStretchData();

private:
    std::string m_matdir;
};