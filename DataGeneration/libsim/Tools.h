//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

#pragma once
#include "CommonIncludes.h"

namespace Tools
{
    DenseMatrixX loadCsvCouble(const std::string &path);
    DenseMatrixXi loadCsvInt(const std::string &path);
    void writeMatrix(const DenseMatrixX &m, const std::string &b);
}
