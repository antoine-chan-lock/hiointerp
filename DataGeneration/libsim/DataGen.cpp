//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

#include "DataGen.h"

DataGen::DataGen(const std::string &matdir)
{
    m_matdir = matdir;
}

DataGen::~DataGen()
{
}

double DataGen::energy(const Vec3 &sca) const
{
    Object obj(m_matdir);
    obj.updateXcoarseSCA(sca);
    VectorX vXfine0 = obj.vectorize(obj.getXcoarse()) * 0;
    VectorX vXfine = obj.NewtonRaphson(vXfine0);
    return obj.getEnergyMembrane(obj.devectorize(vXfine) + obj.getXcoarse());
}

Vec3 DataGen::dpsidsca(const Vec3 &sca) const
{
    // Finite differences over sca
    double enerp, ener;
    Vec3 vP;
    Vec3 dsca;
    double h = 0.00001;
    vP.setZero();
    ener = energy(sca);
    for (int i = 0; i < 3; i++)
    {
        dsca.setZero();
        dsca(i) = h;
        enerp = energy(sca + dsca);
        vP(i) = (enerp - ener) / h;
    }
    return vP;
}

Vec3 DataGen::SCA2vE(const Vec3 &sca) const
{
    Matrix2 S, F, U;
    S << 1 + sca(0), 0,
        0, 1 - sca(1);
    double angler = sca(2) * atan(1) * 4 / 180;
    U << cos(angler), -sin(angler),
        sin(angler), cos(angler);
    F = U * S * U.transpose();

    Matrix2 E = 0.5 * (F.transpose() * F - DenseMatrixX::Identity(2, 2));
    Vec3 vE(E(0, 0), E(1, 1), E(0, 1) * 2.0);
    return vE;
}

Matrix3 DataGen::dscadvE(const Vec3 &sca) const
{
    // Computed dvEdsca with Matlab (see dsca2vE.m)
    // dscadvE = inv(dvEdsca)
    double s = sca(0);
    double c = sca(1);
    double a = sca(2);
    double pi = atan(1) * 4;
    Matrix3 res;
    res(0, 0) = -pow(cos((a * pi) / 1.8E+2), 2.0) * (pow(sin((a * pi) / 1.8E+2), 2.0) * (c - 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0));
    res(0, 1) = pow(sin((a * pi) / 1.8E+2), 2.0) * (pow(sin((a * pi) / 1.8E+2), 2.0) * (c - 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0));
    res(0, 2) = ((pi * cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0)) / 9.0E+1 + (pi * cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0)) / 9.0E+1) * (pow(sin((a * pi) / 1.8E+2), 2.0) * (c - 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) + (cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0)) * ((pi * pow(cos((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) / 1.8E+2 - (pi * pow(sin((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) / 1.8E+2 + (pi * pow(cos((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) / 1.8E+2 - (pi * pow(sin((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) / 1.8E+2);
    res(1, 0) = pow(sin((a * pi) / 1.8E+2), 2.0) * (pow(sin((a * pi) / 1.8E+2), 2.0) * (s + 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0));
    res(1, 1) = -pow(cos((a * pi) / 1.8E+2), 2.0) * (pow(sin((a * pi) / 1.8E+2), 2.0) * (s + 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0));
    res(1, 2) = ((pi * cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0)) / 9.0E+1 + (pi * cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0)) / 9.0E+1) * (pow(sin((a * pi) / 1.8E+2), 2.0) * (s + 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) + (cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0)) * ((pi * pow(cos((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) / 1.8E+2 - (pi * pow(sin((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) / 1.8E+2 + (pi * pow(cos((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) / 1.8E+2 - (pi * pow(sin((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) / 1.8E+2);
    res(2, 0) = pow(cos((a * pi) / 1.8E+2), 2.0) * (cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0)) + pow(sin((a * pi) / 1.8E+2), 2.0) * (cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0)) - cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (pow(sin((a * pi) / 1.8E+2), 2.0) * (c - 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (pow(sin((a * pi) / 1.8E+2), 2.0) * (s + 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (c - 1.0));
    res(2, 1) = -pow(cos((a * pi) / 1.8E+2), 2.0) * (cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0)) - pow(sin((a * pi) / 1.8E+2), 2.0) * (cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (c - 1.0) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (s + 1.0)) - cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (pow(sin((a * pi) / 1.8E+2), 2.0) * (c - 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) + cos((a * pi) / 1.8E+2) * sin((a * pi) / 1.8E+2) * (pow(sin((a * pi) / 1.8E+2), 2.0) * (s + 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (c - 1.0));
    res(2, 2) = -(pow(sin((a * pi) / 1.8E+2), 2.0) * (c - 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) * ((pi * pow(cos((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) / 1.8E+2 - (pi * pow(sin((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) / 1.8E+2 + (pi * pow(cos((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) / 1.8E+2 - (pi * pow(sin((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) / 1.8E+2) + (pow(sin((a * pi) / 1.8E+2), 2.0) * (s + 1.0) - pow(cos((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) * ((pi * pow(cos((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) / 1.8E+2 - (pi * pow(sin((a * pi) / 1.8E+2), 2.0) * (c - 1.0)) / 1.8E+2 + (pi * pow(cos((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) / 1.8E+2 - (pi * pow(sin((a * pi) / 1.8E+2), 2.0) * (s + 1.0)) / 1.8E+2);
    Matrix3 resInv = res.inverse();
    if (std::isnan(resInv.sum()))
    {
        std::cout << "resInv is nan" << std::endl;
        exit(0);
    }
    return res.inverse();
}

double DataGen::gradDesc(const Vec3 &sca_in) const
{
    // We search the compression value that minimizes the energy
    // We use a gradient descent method with FD
    // We limit the step size to 0.1 MAX to ensure the deformations make sense
    // We use line search to find the step size
    double h = 1e-3;
    int maxIter = 1e5, iter = 0;
    double alphaMax = 1e13, maxError = 1e-2, error = 1e2;
    Vec3 sca = sca_in, scaGuess, dsca;
    double ener = energy(sca);
    while ((iter < maxIter) and (error > maxError))
    {
        double cguess;
        // compute FD gradient
        dsca.setZero();
        dsca(1) = h;
        double enerp = energy(sca + dsca);
        double enerm = energy(sca - dsca);
        double grad = (enerp - enerm) / (2 * h);
        error = abs(grad);
        // clamping the gradient
        if (abs(grad) > 0.1)
            grad = 0.1 * (grad / abs(grad));
        // line search
        double alpha = 1;
        double energyGuess = 1e10;
        while (energyGuess > ener)
        {
            cguess = sca(1) - grad / alpha;
            scaGuess << sca(0), cguess, sca(2);
            energyGuess = energy(scaGuess);
            alpha = alpha * 2;
            if (alpha > alphaMax)
            {
                std::cout << "orthosearch: alpha went too big" << std::endl;
                std::cout << "sca : " << scaGuess << std::endl;
                exit(0);
            }
        }
        sca(1) = cguess;
        ener = energyGuess;
        iter++;
    }
    return sca(1);
}

void DataGen::genBenStretchData()
{
    // Subdivision of the sampling space
    Eigen::ArrayXd vangle = Eigen::ArrayXd::LinSpaced(18, 0, 170);
    Eigen::ArrayXd vamp = Eigen::ArrayXd::LinSpaced(10, 0.05, 0.7);

    // stretch
    int maxidx = vangle.rows() * vamp.rows();
    std::vector<Vec3> v_sca(maxidx + 1);
    std::vector<Vec3> v_vE(maxidx + 1);

    // Energy
    std::vector<double> v_energy(maxidx + 1);

    // Stress
    std::vector<Vec3> v_dpsidE(maxidx + 1);
    std::vector<Vec3> v_dpsidSCA(maxidx + 1);

    int cpt = 0;
#pragma omp parallel for
    for (int i = 0; i < vangle.rows(); i++)
    {
        Vec3 vE;
        Vec3 sca;
        double ortho_response = 0;
        // As we stretch from 0, we expect the orthogonal response to start from 0 then follow the stretch response
        // We reuse the orthogonal response as a guess for the next one
        for (int j = 0; j < vamp.rows(); j++)
        {
            ortho_response = 0;
            sca << vamp(j), ortho_response, vangle(i);
            ortho_response = gradDesc(sca);
            sca << vamp(j), ortho_response, vangle(i);
            int idx = j + i * vamp.rows();
            v_sca[idx] = sca;
            v_vE[idx] = SCA2vE(sca);
            v_energy[idx] = energy(sca);
            v_dpsidSCA[idx] = dpsidsca(sca);
            v_dpsidE[idx] = dscadvE(sca) * v_dpsidSCA[idx];
            Object obj(m_matdir);
            obj.updateXcoarseSCA(sca);
            VectorX vXfine0 = obj.vectorize(obj.getXcoarse()) * 0;
            VectorX vXfine = obj.NewtonRaphson(vXfine0);
            Tools::writeMatrix(obj.devectorize(vXfine) + obj.getXcoarse(), m_matdir + "/Xdef/X" + std::to_string(idx) + ".csv");
        }
        std::cout << i << "/" << vangle.rows() << std::endl;
    }
    Vec3 sca;
    sca << 0, 0, 0;
    v_energy[maxidx] = energy(sca);
    DenseMatrixX res;
    res.setZero(v_energy.size(), 13);
    // Ex Ey Exy s c a energy
    for (int i = 0; i < v_energy.size(); i++)
    {
        res(i, 0) = v_sca[i](0);
        res(i, 1) = v_sca[i](1);
        res(i, 2) = v_sca[i](2);

        res(i, 3) = v_vE[i](0);
        res(i, 4) = v_vE[i](1);
        res(i, 5) = v_vE[i](2);

        res(i, 6) = v_energy[i];

        res(i, 7) = v_dpsidSCA[i](0);
        res(i, 8) = v_dpsidSCA[i](1);
        res(i, 9) = v_dpsidSCA[i](2);

        res(i, 10) = v_dpsidE[i](0);
        res(i, 11) = v_dpsidE[i](1);
        res(i, 12) = v_dpsidE[i](2);
    }
    Tools::writeMatrix(res, m_matdir + "/data.csv");
}
