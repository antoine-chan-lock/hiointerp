//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

#include "Tools.h"

DenseMatrixX Tools::loadCsvCouble(const std::string &path)
{
    std::ifstream indata;
    indata.open(path);
    std::string line;
    std::vector<double> values;
    int rows = 0;
    while (getline(indata, line))
    {
        std::stringstream lineStream(line);
        std::string cell;
        while (getline(lineStream, cell, ','))
        {
            values.push_back(stod(cell));
        }
        ++rows;
    }
    return Eigen::Map<const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(values.data(), rows, values.size() / rows);
}

DenseMatrixXi Tools::loadCsvInt(const std::string &path)
{
    std::ifstream indata;
    indata.open(path);
    std::string line;
    std::vector<int> values;
    int rows = 0;
    while (getline(indata, line))
    {
        std::stringstream lineStream(line);
        std::string cell;
        while (getline(lineStream, cell, ','))
        {
            values.push_back(stod(cell));
        }
        ++rows;
    }
    return Eigen::Map<const Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(values.data(), rows, values.size() / rows);
}

void Tools::writeMatrix(const DenseMatrixX &m, const std::string &b)
{
    const static Eigen::IOFormat CSVFormat(Eigen::FullPrecision, Eigen::DontAlignCols, ", ", "\n");
    std::ofstream file(b);
    file << m.format(CSVFormat);
}