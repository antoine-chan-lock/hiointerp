//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

#include "Element.h"

Element::Element()
{
}

Element::Element(int id, const DenseMatrixXi &connectivity, const DenseMatrixX &Xrest)
{
    m_id = id;
    m_nodes = connectivity.row(id);
    m_material.init(1000, 0.3); // hardcoded material properties
    m_Xrest.col(0) = Xrest(m_nodes(0), Eigen::all);
    m_Xrest.col(1) = Xrest(m_nodes(1), Eigen::all);
    m_Xrest.col(2) = Xrest(m_nodes(2), Eigen::all);
    m_Xrest.col(3) = Xrest(m_nodes(3), Eigen::all);
    m_nbNodes = 4;
    m_nbDofs = m_nbNodes * 2;
    Eigen::Matrix<double, 4, 2> quadCoord; // Coordinates of the quadrature points
    double pos = 1.0 / sqrt(3);
    quadCoord << -pos, -pos,
        pos, -pos,
        pos, pos,
        -pos, pos;

    for (int i = 0; i < 4; i++)
    {
        m_dN[i](0, 0) = -(1 - quadCoord(i, 1));
        m_dN[i](1, 0) = (1 - quadCoord(i, 1));
        m_dN[i](2, 0) = (1 + quadCoord(i, 1));
        m_dN[i](3, 0) = -(1 + quadCoord(i, 1));

        m_dN[i](0, 1) = -(1 - quadCoord(i, 0));
        m_dN[i](1, 1) = -(1 + quadCoord(i, 0));
        m_dN[i](2, 1) = (1 + quadCoord(i, 0));
        m_dN[i](3, 1) = (1 - quadCoord(i, 0));

        m_dN[i] = m_dN[i] / 4;

        m_dFdx[i] = m_dN[i] * (m_Xrest * m_dN[i]).inverse();

        m_dvFdvx[i].setZero();

        m_dvFdvx[i] << m_dFdx[i](0, 0), 0, m_dFdx[i](1, 0), 0, m_dFdx[i](2, 0), 0, m_dFdx[i](3, 0), 0,
            0, m_dFdx[i](0, 0), 0, m_dFdx[i](1, 0), 0, m_dFdx[i](2, 0), 0, m_dFdx[i](3, 0),
            m_dFdx[i](0, 1), 0, m_dFdx[i](1, 1), 0, m_dFdx[i](2, 1), 0, m_dFdx[i](3, 1), 0,
            0, m_dFdx[i](0, 1), 0, m_dFdx[i](1, 1), 0, m_dFdx[i](2, 1), 0, m_dFdx[i](3, 1);

        Matrix2 jac = m_Xrest * m_dN[i];

        m_detJac[i] = jac.determinant();
    }
}

Element::~Element()
{
}

Vec8iT Element::getIndexDoFs() const
{
    Vec8iT res;
    res << 2 * m_nodes(0), 2 * m_nodes(0) + 1,
        2 * m_nodes(1), 2 * m_nodes(1) + 1,
        2 * m_nodes(2), 2 * m_nodes(2) + 1,
        2 * m_nodes(3), 2 * m_nodes(3) + 1;
    return res;
}

double Element::getEnergy(const DenseMatrixX &Xcurrent) const
{
    // Input: Xcurrent: current position of the nodes
    // Output: energy of the element
    Matrix2_4 Xcurrent_elt;
    Xcurrent_elt.col(0) = Xcurrent(m_nodes(0), Eigen::all);
    Xcurrent_elt.col(1) = Xcurrent(m_nodes(1), Eigen::all);
    Xcurrent_elt.col(2) = Xcurrent(m_nodes(2), Eigen::all);
    Xcurrent_elt.col(3) = Xcurrent(m_nodes(3), Eigen::all);
    Matrix2 F;
    double res = 0;
    for (int i = 0; i < 4; i++)
    {
        F = Xcurrent_elt * m_dFdx[i];
        res += m_detJac[i] * m_material.psi(F);
    }
    return res;
}

Vec8T Element::getGradient(const DenseMatrixX &Xcurrent) const
{
    // Input: Xcurrent: current position of the nodes
    // Output: gradient of the element
    Matrix2_4 Xcurrent_elt;
    Xcurrent_elt.col(0) = Xcurrent(m_nodes(0), Eigen::all);
    Xcurrent_elt.col(1) = Xcurrent(m_nodes(1), Eigen::all);
    Xcurrent_elt.col(2) = Xcurrent(m_nodes(2), Eigen::all);
    Xcurrent_elt.col(3) = Xcurrent(m_nodes(3), Eigen::all);
    Matrix2 F;
    Vec8T res;
    res.setZero();
    for (int i = 0; i < 4; i++)
    {
        F = Xcurrent_elt * m_dFdx[i];
        res += m_detJac[i] * m_material.dpsidF(F) * m_dvFdvx[i];
    }
    return res;
}

Matrix8 Element::getHessian(const DenseMatrixX &Xcurrent) const
{
    // Input: Xcurrent: current position of the nodes
    // Output: hessian of the element
    Matrix2_4 Xcurrent_elt;
    Xcurrent_elt.col(0) = Xcurrent(m_nodes(0), Eigen::all);
    Xcurrent_elt.col(1) = Xcurrent(m_nodes(1), Eigen::all);
    Xcurrent_elt.col(2) = Xcurrent(m_nodes(2), Eigen::all);
    Xcurrent_elt.col(3) = Xcurrent(m_nodes(3), Eigen::all);
    Matrix2 F;
    Matrix8 res;
    res.setZero();
    for (int i = 0; i < 4; i++)
    {
        F = Xcurrent_elt * m_dFdx[i];
        res += m_detJac[i] * m_dvFdvx[i].transpose() * m_material.dpsidFF(F) * m_dvFdvx[i];
    }
    return res;
}

int Element::checkInversion(const DenseMatrixX &Xcurrent) const
{
    // Input: Xcurrent: current position of the nodes
    // Output: 1 if inversion, 0 otherwise
    Matrix2_4 Xcurrent_elt;
    Xcurrent_elt.col(0) = Xcurrent(m_nodes(0), Eigen::all);
    Xcurrent_elt.col(1) = Xcurrent(m_nodes(1), Eigen::all);
    Xcurrent_elt.col(2) = Xcurrent(m_nodes(2), Eigen::all);
    Xcurrent_elt.col(3) = Xcurrent(m_nodes(3), Eigen::all);
    Matrix2 F;
    int res = 0;
    for (int i = 0; i < 4; i++)
    {
        F = Xcurrent_elt * m_dFdx[i];
        if (F.determinant() < 0.001)
            res = 1;
    }
    return res;
}