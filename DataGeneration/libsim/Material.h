//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

#pragma once
#include "CommonIncludes.h"

// Stable Neo-Hookean material from "Stable Neo-Hookean Flesh Simulation" by B. SMITH
class Material
{
public:
    Material();
    virtual ~Material();
    virtual void init(double lambda, double mu);
    virtual double psi(const Matrix2 &F) const;
    virtual Vec4T dpsidF(const Matrix2 &F) const;
    virtual Matrix4 dpsidFF(const Matrix2 &F) const;
    virtual Matrix4 clamp(const Matrix4 &H) const;

protected:
    double m_young, m_nu, m_G, m_lambda, m_lambdaNH, m_GNH, m_alpha;
};
