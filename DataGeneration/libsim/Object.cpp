//==========================================================
//
//	Simulation library part of High-Order Elasticity Interpolants for Microstructure Simulation
//  mslab.es/projects/HiOInterp/
//
//	Author:
//			Antoine Chan-Lock, URJC, Spain
//
//==========================================================

#include "Object.h"

Object::Object()
{
}

Object::Object(std::string matdir)
{
    m_connectivity = Tools::loadCsvInt(matdir + "/T.csv");
    m_restX = Tools::loadCsvCouble(matdir + "/X.csv");
    m_coarseX = m_restX;
    m_nbNodes = m_restX.rows();
    m_nbElements = m_connectivity.rows();
    m_nbDoFs = m_nbNodes * 2;
    m_PBCDoFs = Tools::loadCsvInt(matdir + "/PBC.csv"); // List of twin nodes
    std::vector<Element> elts;
    for (int i = 0; i < m_connectivity.rows(); i++)
    {
        elts.push_back(Element(i, m_connectivity, m_restX));
    }
    m_velement = elts;
    m_sca.setOnes();
    m_sca *= -1;
}

Object::~Object()
{
}

double Object::getEnergy(const DenseMatrixX &Xfine) const
{
    DenseMatrixX X = Xfine + m_coarseX;
    double res = 0;
    // Energy that fixes the center of mass
    VectorX vX = vectorize(X), vXrest = vectorize(m_restX);
    DenseMatrixX C = DenseMatrixX::Zero(2, m_nbDoFs);
    C(0, Eigen::seq(0, Eigen::last, 2)) = DenseMatrixX::Ones(1, m_nbDoFs / 2);
    C(1, Eigen::seq(1, Eigen::last, 2)) = DenseMatrixX::Ones(1, m_nbDoFs / 2);
    res += 0.5 * (C * vX).transpose() * (C * vX);
    res += -0.5 * (C * vXrest).transpose() * (C * vX);
    res += -0.5 * (C * vX).transpose() * (C * vXrest);
    res += 0.5 * (C * vXrest).transpose() * (C * vXrest);
    // Material energy
    for (int e = 0; e < m_velement.size(); e++)
    {
        res += m_velement[e].getEnergy(X);
    }
    // Penalty energy
    res += penalty(vectorize(Xfine));
    return res;
}

double Object::getEnergy(const VectorX &vXfine) const
{
    return getEnergy(devectorize(vXfine));
}

VectorX Object::getGradient(const DenseMatrixX &Xfine) const
{
    DenseMatrixX X = Xfine + m_coarseX;
    VectorX res = VectorX::Zero(X.rows() * X.cols());
    VectorX vX = vectorize(X), vXrest = vectorize(m_restX);
    // Center of mass gradient
    DenseMatrixX C = DenseMatrixX::Zero(2, m_nbDoFs);
    C(0, Eigen::seq(0, Eigen::last, 2)) = DenseMatrixX::Ones(1, m_nbDoFs / 2);
    C(1, Eigen::seq(1, Eigen::last, 2)) = DenseMatrixX::Ones(1, m_nbDoFs / 2);
    res += (C * vX).transpose() * C;
    res += -(C * vXrest).transpose() * C;
    // Material energy gradient
    res += getGradientMembrane(X);
    // Penalty gradient
    res += dpenalty(vectorize(Xfine));
    return res;
}

SparseMatrixX Object::getHessian(const DenseMatrixX &Xfine) const
{
    DenseMatrixX X = Xfine + m_coarseX;
    // Center of mass hessian (0 and 1 checkerboard)
    DenseMatrixX ConstCenterMass = DenseMatrixX::Zero(2, m_nbDoFs);
    ConstCenterMass(0, Eigen::seq(0, Eigen::last, 2)) = DenseMatrixX::Ones(1, m_nbDoFs / 2);
    ConstCenterMass(1, Eigen::seq(1, Eigen::last, 2)) = DenseMatrixX::Ones(1, m_nbDoFs / 2);
    DenseMatrixX H = ConstCenterMass.transpose() * ConstCenterMass;
    SparseMatrixX mat(m_nbDoFs, m_nbDoFs);
    mat.setZero();
    mat = H.sparseView();
    // Material energy hessian
    mat += getHessianMembrane(X);
    // Penalty hessian
    mat += ddpenalty(vectorize(Xfine));
    return mat;
}

double Object::getEnergyMembrane(const DenseMatrixX &X) const
{
    double res = 0;
    for (int e = 0; e < m_velement.size(); e++)
    {
        res += m_velement[e].getEnergy(X);
    }
    return res;
}

VectorX Object::getGradientMembrane(const DenseMatrixX &X) const
{
    VectorX res = VectorX::Zero(X.rows() * X.cols());
    for (int e = 0; e < m_velement.size(); e++)
    {
        Vec8T localGrad;
        localGrad = m_velement[e].getGradient(X);
        res(m_velement[e].getIndexDoFs()) += localGrad;
    }
    return res;
}

SparseMatrixX Object::getHessianMembrane(const DenseMatrixX &X) const
{
    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;
    tripletList.reserve(8 * 8 * m_nbElements);
    for (int e = 0; e < m_velement.size(); e++)
    {
        Matrix8 localHess = m_velement[e].getHessian(X);
        Vec8iT idx = m_velement[e].getIndexDoFs();
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                tripletList.push_back(T(idx(i), idx(j), localHess(i, j)));
            }
        }
    }
    SparseMatrixX mat(m_nbDoFs, m_nbDoFs);
    mat.setFromTriplets(tripletList.begin(), tripletList.end());
    return mat;
}

double Object::penalty(const VectorX &vXfine) const
{
    double res = 0;
    for (int i = 0; i < m_PBCDoFs.rows(); i++)
    {
        double dx = vXfine(m_PBCDoFs(i, 0));
        double dx2 = vXfine(m_PBCDoFs(i, 1));
        res += 1e8 * pow(dx - dx2, 2);
    }
    return res;
}

VectorX Object::dpenalty(const VectorX &vXfine) const
{
    VectorX res;
    res.setZero(m_nbDoFs);
    for (int i = 0; i < m_PBCDoFs.rows(); i++)
    {
        double dx = vXfine(m_PBCDoFs(i, 0));
        double dx2 = vXfine(m_PBCDoFs(i, 1));
        res(m_PBCDoFs(i, 0)) += 1e8 * 2 * (dx - dx2);
        res(m_PBCDoFs(i, 1)) += -1e8 * 2 * (dx - dx2);
    }
    return res;
}

SparseMatrixX Object::ddpenalty(const VectorX &vXfine) const
{
    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;
    tripletList.reserve(m_PBCDoFs.rows() * 4);
    for (int i = 0; i < m_PBCDoFs.rows(); i++)
    {
        tripletList.push_back(T(m_PBCDoFs(i, 0), m_PBCDoFs(i, 0), 1e8 * 2));
        tripletList.push_back(T(m_PBCDoFs(i, 1), m_PBCDoFs(i, 1), 1e8 * 2));
        tripletList.push_back(T(m_PBCDoFs(i, 0), m_PBCDoFs(i, 1), -1e8 * 2));
        tripletList.push_back(T(m_PBCDoFs(i, 1), m_PBCDoFs(i, 0), -1e8 * 2));
    }
    SparseMatrixX mat(m_nbDoFs, m_nbDoFs);
    mat.setFromTriplets(tripletList.begin(), tripletList.end());
    return mat;
}

DenseMatrixX Object::devectorize(const VectorX &vX) const
{
    DenseMatrixX X = DenseMatrixX::Zero(m_restX.rows(), m_restX.cols());
    X.col(0) = vX(Eigen::seq(0, Eigen::last, 2));
    X.col(1) = vX(Eigen::seq(1, Eigen::last, 2));
    return X;
}

VectorX Object::vectorize(const DenseMatrixX &X) const
{
    VectorX vx = VectorX::Zero(m_nbDoFs);
    vx(Eigen::seq(0, Eigen::last, 2)) = X.col(0);
    vx(Eigen::seq(1, Eigen::last, 2)) = X.col(1);
    return vx;
}

bool Object::checkInversion(const DenseMatrixX &Xfine) const
{
    DenseMatrixX Xcurrent = Xfine + m_coarseX;
    int res = 0;
    for (int elt = 0; elt < m_nbElements; elt++)
    {
        res += m_velement[elt].checkInversion(Xcurrent);
    }
    bool out = false;
    if (res > 0)
        out = true;
    return out;
}

DenseMatrixXi Object::getConnetivity() const
{
    return m_connectivity;
}

VectorX Object::NewtonRaphson(const VectorX &vXfine) const
{
    // We want to minimize f = material energy + penalty + center of mass
    // We compute a guess for the new position of the vertices (xguess = x - df\ddf)
    // We check that the new position has no inversions
    // We check that the energy is decreasing
    // If not, we decrease the step size (linesearch). The energy should always be decreasing as we clamped the hessian
    // We repeat until convergence
    VectorX vXdef = vXfine, vXGuess;
    int maxIter = 1e5, iter = 0;
    double alphaMax = 1e10, maxError = 1e-3, error = 1e2;
    double energy = getEnergy(vXdef);
    while ((iter < maxIter) and (error > maxError))
    {
        VectorX dpsi = getGradient(devectorize(vXdef));
        SparseMatrixX ddpsi = getHessian(devectorize(vXdef));
        Eigen::SparseQR<SparseMatrixX, Eigen::COLAMDOrdering<int>> solver;
        ddpsi.makeCompressed();
        solver.compute(ddpsi);
        if (solver.info() != Eigen::Success)
        {
            std::cout << "QR fail" << std::endl;
            exit(0);
        }
        VectorX vdx = solver.solve(dpsi);
        double alpha = 1e0;
        double energyGuess = 1e10;
        bool inverted = false;
        while (energyGuess > energy || inverted)
        {
            vXGuess = vXdef - vdx / alpha;
            inverted = checkInversion(devectorize(vXGuess));
            energyGuess = getEnergy(vXGuess);
            alpha = alpha * 2;
            if (alpha > alphaMax)
            {
                std::cout << "alpha went too big" << std::endl;
                std::cout << "sca: " << m_sca << std::endl;
                exit(0);
            }
        }
        vXdef = vXGuess;
        energy = getEnergy(vXdef);
        iter += 1;
        error = dpsi.norm();
    }
    double pbcpenalty = penalty(vXdef);
    if (pbcpenalty > 1e-4)
    {
        std::cout << "constraints not respected" << std::endl;
        std::cout << pbcpenalty << std::endl;
        exit(0);
    }
    return vXdef;
}

void Object::updateXcoarseF(const Matrix2 &F)
{
    // Coarse mesh linear transformation
    for (int i = 0; i < m_coarseX.rows(); i++)
        m_coarseX.row(i) = m_coarseX.row(i) * F;
}

void Object::updateXcoarseSCA(const Vec3 &sca)
{
    // Coarse mesh linear transformation given
    // s stretch
    // c compression
    // a angle
    m_sca = sca;
    Matrix2 S, F, U;
    S << 1 + sca(0), 0,
        0, 1 - sca(1);
    double angler = sca(2) * atan(1) * 4 / 180;
    U << cos(angler), -sin(angler),
        sin(angler), cos(angler);
    F = U * S * U.transpose();
    updateXcoarseF(F);
}

DenseMatrixX Object::getXcoarse() const
{
    return m_coarseX;
}