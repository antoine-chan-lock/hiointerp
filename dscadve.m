clear all; close all; clc;

syms s c a pi

S = [1+s 0;
     0 1-c];
angler = a*pi/180; %deg2rad
U = [cos(angler) -sin(angler);
     sin(angler) cos(angler)];
F = U*S*U.';
E = (F.'*F - eye(2))/2;
vE = [E(1,1) E(2,2) E(1,2)*2];

dvEdsca = [diff(vE,s).' diff(vE,c).' diff(vE,a).']

ccode(dvEdsca)