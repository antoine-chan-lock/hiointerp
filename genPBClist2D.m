% Antoine Chan-Lock, HiOInterp paper
% Microstructure Periodic nodes definition

% We want to generate a list of twin DoFs representative of the PBCs
clear all;clc;close all;

% Top and bottom rods are periodic
pbcmap1.a =  [0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              1 2 3 4 5 6 7 8 9 10 11 12 13 14];
            
pbcmap1.b =  [0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              1 2 3 4 5 6 7 8 9 10 11 12 13 14;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0];
% Bottom left and top right rods are periodics
pbcmap2.a =  [0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 10 0 0 0 0 0 0 0 0 0 0;
              0 0 0 9 0 0 0 0 0 0 0 0 0 0;
              0 0 0 8 0 0 0 0 0 0 0 0 0 0;
              4 5 6 7 0 0 0 0 0 0 0 0 0 0;
              3 0 0 0 0 0 0 0 0 0 0 0 0 0;
              2 0 0 0 0 0 0 0 0 0 0 0 0 0;
              1 0 0 0 0 0 0 0 0 0 0 0 0 0];
            
pbcmap2.b =  [0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 10;
              0 0 0 0 0 0 0 0 0 0 0 0 0 9;
              0 0 0 0 0 0 0 0 0 0 0 0 0 8;
              0 0 0 0 0 0 0 0 0 0 4 5 6 7;
              0 0 0 0 0 0 0 0 0 0 3 0 0 0;
              0 0 0 0 0 0 0 0 0 0 2 0 0 0;
              0 0 0 0 0 0 0 0 0 0 1 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0];
% Top left and bottom right rods are periodic
pbcmap3.a =  [0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 10 0 0 0;
              0 0 0 0 0 0 0 0 0 0 9 0 0 0;
              0 0 0 0 0 0 0 0 0 0 8 0 0 0;
              0 0 0 0 0 0 0 0 0 0 7 6 5 4;
              0 0 0 0 0 0 0 0 0 0 0 0 0 3;
              0 0 0 0 0 0 0 0 0 0 0 0 0 2;
              0 0 0 0 0 0 0 0 0 0 0 0 0 1];
            
pbcmap3.b =  [0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              10 0 0 0 0 0 0 0 0 0 0 0 0 0;
              9 0 0 0 0 0 0 0 0 0 0 0 0 0;
              8 0 0 0 0 0 0 0 0 0 0 0 0 0;
              7 6 5 4 0 0 0 0 0 0 0 0 0 0;
              0 0 0 3 0 0 0 0 0 0 0 0 0 0;
              0 0 0 2 0 0 0 0 0 0 0 0 0 0;
              0 0 0 1 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0;
              0 0 0 0 0 0 0 0 0 0 0 0 0 0];
% Number of elements in each periodic rod
pbcmap1.elts = 14;
pbcmap2.elts = 10;
pbcmap3.elts = 10;

% Generating a list of twin elements lelt
lelt=[];
for i=1:pbcmap1.elts
  idx1 = find(rot90(pbcmap1.a,-1)==i);
  idx2 = find(rot90(pbcmap1.b,-1)==i);
  lelt = [lelt; idx1 idx2];
end
for i=1:pbcmap2.elts
  idx1 = find(rot90(pbcmap2.a,-1)==i);
  idx2 = find(rot90(pbcmap2.b,-1)==i);
  lelt = [lelt; idx1 idx2];
end
for i=1:pbcmap3.elts
  idx1 = find(rot90(pbcmap3.a,-1)==i);
  idx2 = find(rot90(pbcmap3.b,-1)==i);
  lelt = [lelt; idx1 idx2];
end

% Given the void / non void element distribution
exist =  [0 0 0 0 0 0 0 0 0 0 0 0 0 0;
          1 1 1 1 1 1 1 1 1 1 1 1 1 1;
          1 0 0 0 0 0 0 0 0 0 0 0 0 1;
          1 0 0 0 0 0 0 0 0 0 0 0 0 1;
          1 1 1 1 0 0 0 0 0 0 1 1 1 1;
          0 0 0 1 0 0 0 0 0 0 1 0 0 0;
          0 0 0 1 0 0 0 0 0 0 1 0 0 0;
          0 0 0 1 0 0 0 0 0 0 1 0 0 0;
          0 0 0 1 0 0 0 0 0 0 1 0 0 0;
          0 0 0 1 0 0 0 0 0 0 1 0 0 0;
          1 1 1 1 0 0 0 0 0 0 1 1 1 1;
          1 0 0 0 0 0 0 0 0 0 0 0 0 1;
          1 0 0 0 0 0 0 0 0 0 0 0 0 1;
          1 1 1 1 1 1 1 1 1 1 1 1 1 1];
exist90 = rot90(exist,-1);
% We build a map that connects ElementNumberingX and ElementNumberingReduced
% Reduced being without void elements
redmap=[];
cpt=1;
for i=1:(size(exist90,1)*size(exist90,2))
  if exist90(i)==1
    redmap=[redmap cpt];
    cpt=cpt+1;
  else
    redmap=[redmap 0];
  end
end

% We load the node position matrix and the connectivity matrix generated
% from genHexMesh.m
X=load("./X.csv");
T=load("./T.csv");
T=T+1;
% We generate a list of twin nodes
lnode = [];
for i=1:size(lelt,1)
  lnode = [lnode; T(redmap(lelt(i,1)),:)' T(redmap(lelt(i,2)),:)'];
end
% We kill duplicates
dbls = [];
for i=1:size(lnode,1)
  dbls = [dbls 10000*lnode(i,1)+lnode(i,2)];
end
[~,idu] = unique(dbls);
lnode = lnode(idu,:);
% We generate a list of twin DoFs called PBC to describe the periodic
% boundary condition. If DoF x1 needs to be the same as DoF x2, there will be
% a line PBC(...,:) = [x1 x2]
PBC = [2*(lnode-1);2*(lnode-1)+1];
dlmwrite("./PBC.csv",PBC,'delimiter',',','precision',5)




