% Antoine Chan-Lock, HiOInterp paper
% Microstructure 10 generation

clear all; clc; close all;
[X,T] = createHEXmesh([-1 1 -1 1],14,14);
% Define void / non void elements
exist =  [0 0 0 0 0 0 0 0 0 0 0 0 0 0;
          1 1 1 1 1 1 1 1 1 1 1 1 1 1;
          1 0 0 0 0 0 0 0 0 0 0 0 0 1;
          1 0 0 0 0 0 0 0 0 0 0 0 0 1;
          1 1 1 1 0 0 0 0 0 0 1 1 1 1;
          0 0 0 1 0 0 0 0 0 0 1 0 0 0;
          0 0 0 1 0 0 0 0 0 0 1 0 0 0;
          0 0 0 1 0 0 0 0 0 0 1 0 0 0;
          0 0 0 1 0 0 0 0 0 0 1 0 0 0;
          0 0 0 1 0 0 0 0 0 0 1 0 0 0;
          1 1 1 1 0 0 0 0 0 0 1 1 1 1;
          1 0 0 0 0 0 0 0 0 0 0 0 0 1;
          1 0 0 0 0 0 0 0 0 0 0 0 0 1;
          1 1 1 1 1 1 1 1 1 1 1 1 1 1];
% Kill void elements
T = killVoid(exist,T);
% Kill void nodes
lexist = [];
for i=1:size(X,1)
  if ismember(i,T)
    lexist=[lexist i];
  end
end
X = X(lexist,:);
% Update node numbering in connectivity matrix
for i=1:( size(T,1)*size(T,2) )
  idx = find( T(i)==lexist );
  T(i)=idx;
end
%write nodes positions X and connectivity matrix T
dlmwrite("./X.csv",X,'delimiter',',','precision',5)
dlmwrite("./T.csv",T-1,'delimiter',',','precision',5)



function T = killVoid(exist,T)
  exist90 = rot90(exist,-1);
  Tp=[];
  for i=1:(14*14)
    if exist90(i)==1
      Tp = [Tp; T(i,:)];
    end
  end
  T=Tp;
end

function [X,T] = createHEXmesh(dom,nx,ny)
  % IN: 2D domain, x subdivisions, y subdivisions
  % OUT: Nodes positions X and hexahedral connectivity matrix T
  npx = nx+1; npy = ny+1; npt = npx * npy; x1 = dom(1); x2 = dom(2);
  y1 = dom(3); y2 = dom(4); x = linspace(x1,x2,npx);  y = linspace(y1,y2,npy); 
  [x,y] = meshgrid(x,y);  X = [reshape(x',npt,1), reshape(y',npt,1)]; 
  T = zeros(nx*ny,4);
  for i=1:ny
    for j=1:nx
      ielem = (i-1)*nx+j; inode = (i-1)*(npx)+j;
      T(ielem,:) = [inode   inode+1   inode+npx+1   inode+npx];
    end
  end
end