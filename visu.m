clear all; close all; clc;

% See each configuration of the samples - comment if necessary
for i=1:179
  X = load("./DataGeneration/data/10/Xdef/X"+num2str(i-1)+".csv");
  scatter(X(:,1),X(:,2))
  xlabel("x")
  ylabel("y")
  xlim([-2 2])
  ylim([-2 2])
  pause
  cla
end

% See the data
data = load("./DataGeneration/data/10/data.csv");
Ex = data(:,4);
Ey = data(:,5);
Ez = data(:,6);
ener = data(:,7);
scatter3(Ex,Ey,Ez,[],ener)